Rails.application.routes.draw do
  resources :statuses
  post '/signup', to: 'users#create'

  post '/login', to: 'authentication#authenticate'

  post '/requests', to: 'requests#create'

  get '/features', to: 'features#index'

  get '/history', to: 'statuses#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
