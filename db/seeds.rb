# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

UserRole.create([
  {role: 'student'},
  {role: 'teacher'},
  {role: 'entity'}
])

ContractType.create([
  {contract_type: 'annual'},
  {contract_type: 'semiannual'}
])

Position.create([
  {position: 'top'},
  {position: 'bottom'}
])

Place.create([
  {place: 'inside'},
  {place: 'outside'}
])

Place.all.to_a.product(Position.all.to_a).each do |place,position|
  Feature.create(place_id: place.id, position_id: position.id)
end

if Rails.env.development?
  User.create(email:'jotaf.daniel@gmail.com', name:'João Daniel', nusp:'7578279', user_role_id:1, password:'12345678', password_confirmation:'12345678')

  Request.create(renew: false, contract_type_id: 1)

  RequestSuggestFeature.create(request_id: 2, feature_id: 2, priority: 3)

  UserMakeRequest.create(user_id: 1, request_id: 1)
end
