class CreateUserMakeRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :user_make_requests do |t|
      t.references :user, foreign_key: true
      t.references :request, foreign_key: true

      t.timestamps
    end
    add_index :user_make_requests, [:user_id, :request_id], unique: true
  end
end
