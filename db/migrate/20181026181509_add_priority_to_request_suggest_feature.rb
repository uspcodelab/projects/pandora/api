class AddPriorityToRequestSuggestFeature < ActiveRecord::Migration[5.2]
  def change
    add_column :request_suggest_features, :priority, :integer
  end
end
