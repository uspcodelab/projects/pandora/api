class AddPasswordToUser < ActiveRecord::Migration[5.2]
  def up
    change_table :users do |t|
      t.string :password_digest      
    end
  end

  def down
    remove_columns :users, :password_digest
  end
end
