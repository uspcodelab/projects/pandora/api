class CreateContractTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :contract_types do |t|
      t.string :contract_type
      t.index :contract_type, unique: true

      t.timestamps
    end
  end
end
