class CreateLockers < ActiveRecord::Migration[5.2]
  def change
    create_table :lockers do |t|
      t.string :number
      t.boolean :available
      t.boolean :active

      t.timestamps
    end
    add_index :lockers, :number, unique: true
  end
end
