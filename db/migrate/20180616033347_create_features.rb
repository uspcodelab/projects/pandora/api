class CreateFeatures < ActiveRecord::Migration[5.2]
  def change
    create_table :features do |t|
      t.references :position, foreign_key: true
      t.references :place, foreign_key: true

      t.timestamps
    end
    add_index :features, [:position_id, :place_id], unique: true
  end
end
