class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email
      t.string :name
      t.string :nusp
      t.references :user_role, foreign_key: true
      t.index :email, unique: true
      t.index :nusp, unique: true

      t.timestamps
    end
  end
end
