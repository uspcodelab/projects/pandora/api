class CreateRequestSuggestFeatures < ActiveRecord::Migration[5.2]
  def change
    create_table :request_suggest_features do |t|
      t.references :request, foreign_key: true
      t.references :feature, foreign_key: true

      t.timestamps
    end
    add_index :request_suggest_features, [:request_id, :feature_id], unique: true
  end
end
