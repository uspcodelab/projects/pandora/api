class CreateLockerHasFeatures < ActiveRecord::Migration[5.2]
  def change
    create_table :locker_has_features do |t|
      t.references :locker, foreign_key: true
      t.references :feature, foreign_key: true

      t.timestamps
    end
  end
end
