class CreatePlaces < ActiveRecord::Migration[5.2]
  def change
    create_table :places do |t|
      t.string :place
      t.index :place, unique: true

      t.timestamps
    end
  end
end
