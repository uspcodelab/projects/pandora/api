# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_08_200628) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "contract_types", force: :cascade do |t|
    t.string "contract_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_type"], name: "index_contract_types_on_contract_type", unique: true
  end

  create_table "features", force: :cascade do |t|
    t.bigint "position_id"
    t.bigint "place_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["place_id"], name: "index_features_on_place_id"
    t.index ["position_id", "place_id"], name: "index_features_on_position_id_and_place_id", unique: true
    t.index ["position_id"], name: "index_features_on_position_id"
  end

  create_table "locker_has_features", force: :cascade do |t|
    t.bigint "locker_id"
    t.bigint "feature_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feature_id"], name: "index_locker_has_features_on_feature_id"
    t.index ["locker_id"], name: "index_locker_has_features_on_locker_id"
  end

  create_table "lockers", force: :cascade do |t|
    t.string "number"
    t.boolean "available"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["number"], name: "index_lockers_on_number", unique: true
  end

  create_table "places", force: :cascade do |t|
    t.string "place"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["place"], name: "index_places_on_place", unique: true
  end

  create_table "positions", force: :cascade do |t|
    t.string "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["position"], name: "index_positions_on_position", unique: true
  end

  create_table "request_suggest_features", force: :cascade do |t|
    t.bigint "request_id"
    t.bigint "feature_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "priority"
    t.index ["feature_id"], name: "index_request_suggest_features_on_feature_id"
    t.index ["request_id", "feature_id"], name: "index_request_suggest_features_on_request_id_and_feature_id", unique: true
    t.index ["request_id"], name: "index_request_suggest_features_on_request_id"
  end

  create_table "requests", force: :cascade do |t|
    t.boolean "renew"
    t.bigint "contract_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contract_type_id"], name: "index_requests_on_contract_type_id"
  end

  create_table "statuses", force: :cascade do |t|
    t.text "text"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_statuses_on_user_id"
  end

  create_table "user_make_requests", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "request_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["request_id"], name: "index_user_make_requests_on_request_id"
    t.index ["user_id", "request_id"], name: "index_user_make_requests_on_user_id_and_request_id", unique: true
    t.index ["user_id"], name: "index_user_make_requests_on_user_id"
  end

  create_table "user_roles", force: :cascade do |t|
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role"], name: "index_user_roles_on_role", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.string "nusp"
    t.bigint "user_role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["nusp"], name: "index_users_on_nusp", unique: true
    t.index ["user_role_id"], name: "index_users_on_user_role_id"
  end

  add_foreign_key "features", "places"
  add_foreign_key "features", "positions"
  add_foreign_key "locker_has_features", "features"
  add_foreign_key "locker_has_features", "lockers"
  add_foreign_key "request_suggest_features", "features"
  add_foreign_key "request_suggest_features", "requests"
  add_foreign_key "requests", "contract_types"
  add_foreign_key "statuses", "users"
  add_foreign_key "user_make_requests", "requests"
  add_foreign_key "user_make_requests", "users"
  add_foreign_key "users", "user_roles"
end
