# Pandora System (API)

API for the Pandora locker management system.

For further info, check the contribution guidelines at [CONTRIBUTING.md][1].

[1]: https://gitlab.com/uspcodelab/projects/pandora/api/blob/master/CONTRIBUTING.md

```
$ docker-compose build
$ docker-compose up
```

Se der um problema de "pid":
Vá até a pasta api/tmp/pids e delete o arquivo server.pid

Em outro terminal( se der problema de armarioscamat_developer): 

```
$ docker exec -it pandora_api_1 bundle exec rake db:create
$ docker exec -it pandora_api_1 bin/rails db:migrate RAILS_ENV=development
$ docker exec -it pandora_api_1 bundle exec rake db:seed
```

Para acessar o rails console (útil para testar a api e o db diretamente):

`
$ docker exec -it pandora_api_1 bin/rails console
`

Para, por exemplo, exibir todos os usuários:
`
> User.all
`