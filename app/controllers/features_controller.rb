class FeaturesController < ApplicationController
  def index
    @positions = Position.all
    @places = Place.all
    @features = Feature.all

    render json: {
      places: @places,
      positions: @positions,
      features: @features
    }
  end
end