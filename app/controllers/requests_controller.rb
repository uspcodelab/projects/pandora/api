class RequestsController < ApplicationController
  before_action :set_request, only: [:show, :update, :destroy]

  # GET /requests
  def index
    @requests = Request.all

    render json: @requests
  end

  # GET /requests/1
  def show
    render json: @request
  end

  # POST /requests
  def create

    @request = Request.new
    @request.renew = request_params[:renew]
    /a sintaxe a seguir não funciona, então tivemos que fazer uma gambiarra por enquanto:
    @user.contract_type = ContractType.find_by_contract_type(request_params[:contract_type])/
    if request_params[:contract_type] == 'annual'
      @request.contract_type =  ContractType.find_by_id(1)
    elsif request_params[:contract_type] == 'semiannual'
      @request.contract_type =  ContractType.find_by_id(2)
    end

    begin 
      ActiveRecord::Base.transaction do
        @request.save!
        /params = request_suggest_feature_params
        params[:selectedPreferences] = @request/
        @request_suggest_feature = RequestSuggestFeature.new
        @request_suggest_feature.request = @request;
        
        /na ideia original, request_suggest_feature teria as colunas de id_do_pedido (que foi 
        atribuído na linha acima), e características do armário desejado. Como no novo esquema
          os pedidos não são feitos em cima de uma única característica, e sim numa ordem de
          prioridades em características, vamos mudar isso/

        preferences = request_params[:selectedPreferences]
        
        /procura pela preferencia de prioridade máxima para colocar no request_suggest_feature/
        for i in 0..preferences.length
          object = preferences[i]
          if object[:priority] == 1
            break
          end
        end

        @request_suggest_feature.feature = Feature.find_by_id(object[:feature])
        @request_suggest_feature.save! #SELECT COUNT(*)#

        /params = user_make_request_params
        params[:request] = @request/

        @user_make_request = UserMakeRequest.new
        @user_make_request.request = @request
        @user_make_request.user = User.find_by_email(request_params[:user])

        @user_make_request.save!
      end
      render json: @request, status: :created
      rescue StandardError => e
      puts e
      render json: @request.errors, status: :unprocessable_entity     
    end
  end

  # PATCH/PUT /requests/1
  def update
    puts("update")
    if @request.update(request_params)
      render json: @request
    else
      render json: @request.errors, status: :unprocessable_entity
    end
  end

  # DELETE /requests/1
  def destroy
    puts("destroy")
    @request.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request
      puts("set_request")
      @request = Request.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def request_params
      params.require(:request).permit(:contract_type, :renew, :user, selectedPreferences: [:feature, :place, :position, :priority])
    end

    /atualmente não sendo usada/
    def request_suggest_feature_params
      params.require(:enabledPreferences)
    end

    /atualmente não sendo usada/
    def user_make_request_params
      params.require(:request).permit(:user_id)
    end
end
