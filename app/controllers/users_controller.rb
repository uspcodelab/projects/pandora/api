class UsersController < ApplicationController
  skip_before_action :authenticate_request, only: [:create]
  before_action :set_user, only: [:show, :update, :destroy]

  class UserCreationError < StandardError; end
  class NuspTakenError < UserCreationError; end
  class EmailTakenError < UserCreationError; end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    prm = user_params
    @user = User.new(prm)
    @user.user_role = UserRole.find_by_role('student')
    
    begin 
      if @user.save
        command = AuthenticateUser.call(prm[:email], prm[:password])
        result = {user: @user, auth_token: command.result}
        status = :created
      else
        raise NuspTakenError, "NUSP já cadastrado" if @user.errors.added? :nusp, :taken
        raise EmailTakenError, "Email já cadastrado" if @user.errors.added? :email, :taken
        raise UserCreationError, "Ocorreu um erro na criação de usuário. Tente novamente mais tarde."
      end
    rescue UserCreationError => e
      result =  { error: e }
      status = :unprocessable_entity
    end
    render json: result, status: status
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:email, :name, :nusp, :password, :password_confirmation)
    end
end
