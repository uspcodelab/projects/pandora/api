class Locker < ApplicationRecord
    has_one :locker_has_feature
    has_many :feature, through: :locker_has_feature
end
