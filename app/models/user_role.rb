class UserRole < ApplicationRecord
  has_many :users
  validates :role, uniqueness: true
end
