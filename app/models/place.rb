class Place < ApplicationRecord
  has_many :features
  validates :place, uniqueness: true
end
