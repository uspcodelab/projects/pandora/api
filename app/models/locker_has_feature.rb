class LockerHasFeature < ApplicationRecord
  belongs_to :locker
  belongs_to :feature
end
