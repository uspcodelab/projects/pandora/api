class Request < ApplicationRecord
  belongs_to :contract_type
  has_many :request_suggest_features
  has_many :features, through: :request_suggest_features
  has_many :user_make_requests
  has_many :users, through: :user_make_requests
end
