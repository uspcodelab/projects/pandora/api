class Feature < ApplicationRecord
  belongs_to :position
  belongs_to :place
  has_many :request_suggest_features
  has_many :requests, through: :request_suggest_features
  has_many :locker_has_features
  has_many :locker, through: :locker_has_features
end
