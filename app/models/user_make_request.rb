class UserMakeRequest < ApplicationRecord
  belongs_to :user
  belongs_to :request
  validate :unique_user_request

  def unique_user_request
    unless UserMakeRequest.where("user_id = ? AND request_id = ?", user_id, request_id).size.eql? 0
      errors.add(:unique_key, "(user_id,request_id) = (#{user_id},#{request_id}) already exists")
    end
  end
end
