class Position < ApplicationRecord
  has_many :features
  validates :position, uniqueness: true
end
