class RequestSuggestFeature < ApplicationRecord
  belongs_to :request
  belongs_to :feature
  validate :unique_request_feature

  def unique_request_feature
    unless RequestSuggestFeature.where("request_id = ? AND feature_id = ?", request_id, feature_id).size.eql? 0
      errors.add(:unique_key, "(request_id,feature_id) = (#{request_id},#{feature_id}) already exists")
    end
  end
end
