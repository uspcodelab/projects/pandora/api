class ContractType < ApplicationRecord
  has_many :requests
  validates :contract_type, uniqueness: true
end
