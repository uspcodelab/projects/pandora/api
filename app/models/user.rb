class User < ApplicationRecord
  has_secure_password
  belongs_to :user_role
  has_many :user_make_requests
  has_many :requests, through: :user_make_requests

  validates :email,
    presence: true, 
    length: { in: 3..254 }, 
    uniqueness: true
  validates :name,  
    presence: true, 
    length: { in:  1..50 }
  validates :nusp,  
    presence: true, 
    length: { in:  6..9  },
    uniqueness: true
  validates :user_role,
    presence: true
end
