# Pandora

Pandora is made with :heart: by [USPCodeLab](https://github.com/uspcodelab)
for the Academic Center (CAMat) of the Institute of Mathematics and Statistics
of the University of São Paulo (IME-USP).

## Requirements

This project uses [Docker](https://docker.com) for development,
test and deployment.

Install Docker by following the instructions in one of the following links,
depending on your OS:
- Mac: https://docs.docker.com/docker-for-mac/install/
- Windows: https://docs.docker.com/docker-for-windows/install/
- Ubuntu: https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/
- Debian: https://docs.docker.com/engine/installation/linux/docker-ce/debian/
- Arch Linux: https://wiki.archlinux.org/index.php/Docker

To make the development easier, the project also uses [Docker Compose][3]
to orchestrate multiple containers together.

Install Docker Compose by following the instructions in
https://docs.docker.com/compose/install/.

## Setup

- To set up the API server for development, run:
  ```
  docker-compose build
  docker-compose run --rm api rails db:setup
  docker-compose up
  ```
  **NOTE:** These commands will **create a Docker image for the API**,
  **set up the database with the latest schema** and **load the server**.
  By default, the API will be available at `localhost:3000`.

## Observations

- The `build` operation may take a while depending on your internet connetion.
  It will download all dependencies specified at `Gemfile` and `Gemfile.lock`
  whenever these files are changed.

- The API uses two databases:
  - [PostgreSQL](https://www.postgresql.org/), an open source relational
    database management system (RDBMS). It is used by Rails to persist objects
    of a given class as tuples in a corresponding table.
  - [Redis](https://redis.io/), an open source in-memory data structure store.
    It is used by Rails to create a cache to speed up HTTP responses.

- The application and the databases are configured with environment variables,
  accordingly to [The Twelve-Factor App](https://12factor.net/). Development
  settings are available in the file `.env`. **These settings must be overriden
  for non-development environments**.

## Known issues

- docker-compose emits a warning when starting the API after the Website:
  ```
  WARNING: Found orphan containers (pandora_web_1) for this project.  If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
  ```
  This is the expected behavior, given that containers in the same
  namespace (`pandora`) but not listed in `docker-compose.yml`
  may be orphans.

- Redis may emit warnings when it is started:
  ```
  WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
  WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
  WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
  ```
  For the development environment, no action is needed. These settings are
  system-wide configurations that cannot be enabled only for the Redis'
  container. For further info, check:
  - https://github.com/docker-library/redis/issues/35
  - https://github.com/docker-library/redis/issues/14
  - https://github.com/docker-library/redis/issues/55
